package simpleex.ui;

import java.net.URL;
import simpleex.restapi.LatLongsService;

public class FxAppUsingRestIT extends AbstractFxAppTest {

  @Override
  protected URL getFxmlResource() {
    return getClass().getResource("FxAppUsingRest.fxml");
  }

  private LatLongsDataAccess dataAccess;

  @Override
  protected LatLongsDataAccess getDataAccess() {
    return dataAccess;
  }

  @Override
  protected void setUpLatLongsDataAccess() {
    final String serverUrlString = "http://localhost:8080/";
    final String clientUrlString = serverUrlString + LatLongsService.LAT_LONG_SERVICE_PATH;
    dataAccess = new RestLatLongsDataAccess(clientUrlString, controller.getObjectMapper());
  }
}
