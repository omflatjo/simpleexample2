# Brukergrensesnittmodulen

Dette prosjektet inneholder brukergrensesnittlaget for [simpleexample2](../README.md).

## Brukergrensesnittlaget

Brukergrensesnittlaget inneholder alle klasser og logikk knyttet til visning og handlinger på dataene i domenelaget. Brukergrensesnittet til vår app viser frem en liste av geo-lokasjoner, den som velges vises på et kart. En flytte og legge til geo-lokasjoner. Samlingen med geo-lokasjoner kan lagres og evt. leses inn igjen.

Brukergrensesnittet er laget med JavaFX og FXML og finnes i **[simpleex.ui](src/main/java/simpleex/ui/README.md)**-pakken (JavaFX-koden i **src/main/java** og FXML-filen i **src/main/resources**)

## Bygging med gradle

Vårt gradle-bygg er satt opp med tillegg for java-applikasjoner generelt (**application**) og med JavaFX spesielt (**org.openjfx.javafxplugin**). Vi trenger (minst) java-versjon 10 og javafx-version 11.

I tillegg bruker vi diverse kodekvalitetsanalyseverktøy ([jacoco](https://github.com/jacoco/jacoco) med **[jacoco](https://docs.gradle.org/current/userguide/jacoco_plugin.html)**, [spotbugs](https://spotbugs.github.io) med **[com.github.spotbugs](https://spotbugs.readthedocs.io/en/latest/gradle.html)** og [checkstyle](https://checkstyle.sourceforge.io) med **[checkstyle](https://docs.gradle.org/current/userguide/checkstyle_plugin.html)**). Disse er satt opp slik at de ikke stopper bygget om ikke alt er på stell. spotbugs er satt opp til å lage rapport med html i stedet for xml.

De fleste avhengighetene hentes fra de vanlige sentrale repo-ene, med unntak av FxMapControl som er lagt ved som jar-fil i **libs**-mappa.
