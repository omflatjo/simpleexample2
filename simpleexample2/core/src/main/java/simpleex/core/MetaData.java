package simpleex.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MetaData {

  /**
   * Name property name.
   */
  public static final String NAME_PROPERTY = "name";
  /**
   * Description property name.
   */
  public static final String DESCRIPTION_PROPERTY = "description";

  /**
   * The list containing standard properties associated to location metadata.
   */
  public static final Collection<String> STD_PROPERTIES = List.of(NAME_PROPERTY, 
      DESCRIPTION_PROPERTY);

  private Collection<String> tags;
  private List<String> properties;

  /**
   * Tells if there are tags and/or properties in this object.
   *
   * @return true if there are tags and/or properties in this object, false
   *         otherwise
   */
  public boolean isEmpty() {
    return (tags == null || tags.isEmpty()) && (properties == null || properties.isEmpty());
  }

  /**
   * Gets the iterator for the tags.
   *
   * @return an iterator for the tags
   */
  public Iterator<String> tags() {
    return (tags != null ? tags.iterator() : Collections.emptyIterator());
  }

  /**
   * Checks if all the provided tags are present.
   *
   * @param tags the tags to check
   * @return true if all the provided tags are present, false otherwise
   */
  public boolean hasTags(final String... tags) {
    if (this.tags == null) {
      return tags.length == 0;
    }
    for (final String tag : tags) {
      if (!this.tags.contains(tag)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Sets the tags to those provided (old ones are removed).
   *
   * @param tags the new tags
   */
  public void setTags(final String... tags) {
    if (tags.length == 0) {
      this.tags = null;
    } else {
      if (this.tags != null) {
        this.tags.clear();
      } else {
        this.tags = new ArrayList<>();
      }
      // cannot use addAll, since tags may contain duplicates
      addTags(tags);
    }
  }

  /**
   * Adds the provided tags.
   *
   * @param tags the tags to add
   */
  public void addTags(final String... tags) {
    if (this.tags == null && tags.length > 0) {
      this.tags = new ArrayList<>();
    }
    // avoid duplicates
    for (final String tag : tags) {
      if (!this.tags.contains(tag)) {
        this.tags.add(tag);
      }
    }
  }

  /**
   * Removes the provided tags.
   *
   * @param tags the tags to remove
   */
  public void removeTags(final String... tags) {
    if (this.tags != null) {
      this.tags.removeAll(Arrays.asList(tags));
      if (this.tags.isEmpty()) {
        this.tags = null;
      }
    }
  }

  /**
   * Gets the iterator for the tags.
   *
   * @return an iterator for the tags
   */
  public Iterator<String> propertyNames() {
    if (properties == null || properties.isEmpty()) {
      return Collections.emptyIterator();
    }
    return new Iterator<String>() {

      int pos = 0;

      @Override
      public boolean hasNext() {
        return properties == null || pos < properties.size();
      }

      @Override
      public String next() {
        final String propertyName = properties.get(pos);
        pos += 2;
        return propertyName;
      }

      @Override
      public void remove() {
        pos -= 2;
        removeProperty(properties.get(pos));
      }
    };
  }

  private int indexOfProperty(final String propertyName) {
    if (properties != null) {
      for (int i = 0; i < properties.size(); i += 2) {
        if (propertyName.equals(properties.get(i))) {
          return i;
        }
      }
    }
    return -1;
  }

  /**
   * Checks if a property with the provided name is present.
   *
   * @param propertyName the name to check
   * @return true if the property is present, false otherwise
   */
  public boolean hasProperty(final String propertyName) {
    return indexOfProperty(propertyName) >= 0;
  }

  /**
   * Gets the property value for the provided property name.
   *
   * @param propertyName the property name
   * @return the value for the provided property name
   */
  public String getProperty(final String propertyName) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      return properties.get(pos + 1);
    }
    return null;
  }

  /**
   * Gets the property value for the provided property name, as an int. If the
   * property value doesn't exist or isn't a valid integer, def is returned.
   *
   * @param propertyName the property name
   * @return the value for the provided property name, as an int, or def it is
   *         missing
   */
  public int getIntegerProperty(final String propertyName, final int def) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      try {
        Integer i = Integer.valueOf(properties.get(pos + 1));
        if (i != null) {
          return i;
        }
      } catch (final NumberFormatException e) {
        // ignore
      }
    }
    return def;
  }

  /**
   * Gets the property value for the provided property name, as a double. If the
   * property value doesn't exist or isn't a valid double, def is returned.
   *
   * @param propertyName the property name
   * @return the value for the provided property name, as a double, or def it is
   *         missing
   */
  public double getDoubleProperty(final String propertyName, final double def) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      try {
        return Double.valueOf(properties.get(pos + 1));
      } catch (final NumberFormatException e) {
        // ignore
      }
    }
    return def;
  }

  /**
   * Gets the property value for the provided property name, as a boolean. If the
   * property value doesn't exist, def is returned.
   *
   * @param propertyName the property name
   * @return the value for the provided property name, as a boolean, or def it is
   *         missing
   */
  public boolean getBooleanProperty(final String propertyName, final boolean def) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      return Boolean.valueOf(properties.get(pos + 1));
    }
    return def;
  }

  /**
   * Sets the property value for the provided property name.
   *
   * @param propertyName  the property name
   * @param propertyValue the (new) property value
   */
  public void setProperty(final String propertyName, final String propertyValue) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      properties.set(pos + 1, propertyValue);
    } else {
      if (properties == null) {
        properties = new ArrayList<>();
      }
      // add the property name
      properties.add(propertyName);
      // and the value
      properties.add(propertyValue);
    }
  }

  /**
   * Convenience method for setting integer property.
   *
   * @param propertyName  the property name
   * @param value the (new) property value
   */
  public void setIntegerProperty(final String propertyName, final int value) {
    setProperty(propertyName, Integer.toString(value));
  }

  /**
   * Convenience method for setting integer property.
   *
   * @param propertyName  the property name
   * @param value the (new) property value
   */
  public void setDoubleProperty(final String propertyName, final double value) {
    setProperty(propertyName, Double.toString(value));
  }

  /**
   * Convenience method for setting boolean property.
   *
   * @param propertyName  the property name
   * @param value the (new) property value
   */
  public void setBooleanProperty(final String propertyName, final boolean value) {
    setProperty(propertyName, Boolean.toString(value));
  }

  /**
   * Removes the property with the provided name.
   *
   * @param propertyName the property to remove.
   */
  public void removeProperty(final String propertyName) {
    final int pos = indexOfProperty(propertyName);
    if (pos >= 0) {
      // remove the property value
      properties.remove(pos + 1);
      // and the name
      properties.remove(pos);
      if (properties.isEmpty()) {
        properties = null;
      }
    }
  }

  /**
   * Check if a certain property is custom or standard a property is standard when
   * the name is found in STD_PROPERTIES.
   *
   * @param propertyName the property name to check
   * @return true if custom false if standard prop
   */
  public static boolean isCustomProperty(String propertyName) {
    return !(STD_PROPERTIES.contains(propertyName));
  }

  /**
   * Convenience method to check if the metadata contains custom properties that
   * is other properties than the name and description.
   *
   * @return true if there are any properties apart from standard ones and false
   *         otherwise
   */
  public boolean hasCustomProperties() {
    for (int i = 0; i < this.properties.size(); i = i + 2) {
      String propertyName = this.properties.get(i);
      if (isCustomProperty(propertyName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Convenience method to check if the metadata contains properties other than
   * the ones given in a list.
   *
   * @return true if there are any properties not in the list, false otherwise
   */
  public boolean hasOtherProperties(String... propertyNames) {
    outer: for (int i = 0; i < this.properties.size(); i = i + 2) {
      String propertyName = this.properties.get(i);
      for (String knownPropertyName : propertyNames) {
        if (propertyName.equals(knownPropertyName)) {
          continue outer;
        }
        return true;
      }
    }
    return false;
  }
}
