package simpleex.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.Iterator;
import simpleex.core.LatLong;
import simpleex.core.MetaData;

public class LatLongSerializer extends JsonSerializer<LatLong> {

  public static final String LONGITUDE_FIELD_NAME = "longitude";
  public static final String LATITUDE_FIELD_NAME = "latitude";
  public static final String META_DATA_FIELD_NAME = "metaData";
  public static final String TAGS_FIELD_NAME = "tags";
  public static final String PROPERTIES_FIELD_NAME = "properties";
  public static final String PROPERTIES_NAME_FIELD_NAME = "name";
  public static final String PROPERTIES_VALUE_FIELD_NAME = "value";

  @Override
  public void serialize(final LatLong latLon, final JsonGenerator jsonGen,
      final SerializerProvider provider) throws IOException {
    jsonGen.writeStartObject();
    jsonGen.writeFieldName(LATITUDE_FIELD_NAME);
    jsonGen.writeNumber(latLon.getLatitude());
    jsonGen.writeFieldName(LONGITUDE_FIELD_NAME);
    jsonGen.writeNumber(latLon.getLongitude());

    // serialize meta-data
    if (latLon.hasMetaData()) {
      jsonGen.writeFieldName(META_DATA_FIELD_NAME);
      jsonGen.writeStartObject();
      final MetaData metaData = latLon.getMetaData();
      final Iterator<String> tags = metaData.tags();
      if (tags.hasNext()) {
        jsonGen.writeFieldName(TAGS_FIELD_NAME);
        jsonGen.writeStartArray();
        while (tags.hasNext()) {
          jsonGen.writeString(tags.next());
        }
        jsonGen.writeEndArray();
      }
      final Iterator<String> propertyNames = metaData.propertyNames();
      if (propertyNames.hasNext()) {
        jsonGen.writeFieldName(PROPERTIES_FIELD_NAME);
        jsonGen.writeStartArray();
        while (propertyNames.hasNext()) {
          jsonGen.writeStartObject();
          jsonGen.writeFieldName(PROPERTIES_NAME_FIELD_NAME);
          final String propertyName = propertyNames.next();
          jsonGen.writeString(propertyName);
          jsonGen.writeFieldName(PROPERTIES_VALUE_FIELD_NAME);
          jsonGen.writeString(metaData.getProperty(propertyName));
          jsonGen.writeEndObject();
        }
        jsonGen.writeEndArray();
      }
      jsonGen.writeEndObject();
    }
    jsonGen.writeEndObject();
  }
}
