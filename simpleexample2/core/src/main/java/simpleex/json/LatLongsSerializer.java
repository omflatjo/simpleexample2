package simpleex.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;

public class LatLongsSerializer extends JsonSerializer<LatLongs> {

  @Override
  public void serialize(final LatLongs latLongs, final JsonGenerator jsonGen,
      final SerializerProvider provider) throws IOException {
    jsonGen.writeStartArray(latLongs.getLatLongCount());
    for (final LatLong latLong : latLongs) {
      jsonGen.writeObject(latLong);
    }
    jsonGen.writeEndArray();
  }
}
