# Modul for web-serveren

Dette prosjektet inneholder web-serveren med REST-api-et for [simpleexample2](../README.md).

## Web-serveren

REST-tjenestelogikken i [restapi-modulen](../restapi/README.md) trenger en web-server for å bli gjort tilgjengelig. Vi må naturlig nok ha en web-server med støtte for [JAX-RS-standarden](https://en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services), siden det er den REST-API-logikken er programmert iht. Det er flere å velge mellom, vi har valgt å bruke [Jersey](https://eclipse-ee4j.github.io/jersey/), som er åpen kildekode (og nylig overført til Eclipse-stiftelsen).

Jersey er i seg selv ikke en komplett web-server, men bygger bro mellom en underliggende HTTP-tjener og JAX-RS-baserte REST-API-klasser. Derfor trenger vi også en ren HTTP-tjener, og av mange alternativer bruker vi her bruker vi [grizzly2](https://javaee.github.io/grizzly/).

Koden består av to "små" klasser:

* **LatLongConfig** registrerer klassene som utgjør og støtter REST-tjenesten vår.
* **LatLongGrizzlyApp** inneholder oppstartskode.

### LatLongConfig

Konfigurasjonsteknikken består i å oppgi hvilke klasser og evt. objekter som "samarbeider" om REST-tjenesten. Hvilken rolle disse spiller bestemmes av typene, Jersey sørger for å instansiere dem og koble instansene sammen. Detaljene trenger vi ikke å kjenne til, vi må bare vite hvilke klasser vi må registrere. Den viktigste er REST-tjenesteklassen **LatLongsService**, mens de to andre er støtteklasser. 

Vi registrerer også et *objekt* (indirekte vha. en AbstractBinder), dette er det ene **LatLongs**-objektet som REST-tjenesten opererer på. Siden typen stemmer overens med det **@Inject**-annoterte *latLongs*-feltet i **LatLongsService**-klassen, vil det bli "injisert" (automagisk satt) og på den måten gjøres tilgjengelig for alle REST-API-metodene.

### LatLongGrizzlyApp 

Denne klassen brukes bare ved oppstart av serveren, f.eks. hvis den kjøres som en selvstendig applikasjon. Klassen har egne metoder for å start og stoppe serveren, og disse brukes av den varianten av JavaFX-app-en som bruker REST-API-et. Oppstartskoden tar i mot to kommandolinjeargumenter for å konfigurere serveren:

* Det første argumentet er URL-en til tjenesten. Her brukes bare port-nummeret og path-delen.
* Det andre argumentet er det initielle **LatLongs**-objektet, kodet som JSON. Uten argumentet brukes et tomt et.  

Serveren kjører som en uavhengig "tråd", slik at den ikke nødvendigvis er kommet helt i gang når koden vår får referansen til det nyopprettede **HttpServer**-objektet (returnert av kallet til **GrizzlyHttpServerFactory.createHttpServer**). Derfor har **startServer**-metoden bygget inn muligheten til å vente e viss tid på at serveren svarer før metoden returnerer.

## Bygging med gradle

Avhengighetene viser at vi bruker en rekke rammeverk og biblioteker. Detaljene har vi lest oss frem til i [dokumentasjonen til Jersey](https://jersey.github.io/documentation/latest/deployment.html#deployment.http).

Versjonsnumrene er viktige, siden alt må spille sammen. For å gjøre det ryddigere er variabler med versjonsnumrene satt i en **ext**-blokk og angitt i identifikasjonsstrengen med $-notasjonen. Merk at for at versjonsvariabelverdiene skal bli "skutt inn" i strengen, så må en bruke " og ikke ' rundt strengen.
